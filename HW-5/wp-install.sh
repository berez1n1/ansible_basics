#!/usr/bin/env bash

set -e

yamllint -c ~/ansible_basics/yamllint-cfg.yaml .
sleep 5
ansible-lint ~/ansible_basics/HW-5/deploy-all.yml
sleep 5
sudo ansible-playbook deploy-all.yml -i inventories/hosts -b --vault-password-file ~/.ansible_pass.txt $@