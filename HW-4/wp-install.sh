#!/usr/bin/env bash

sudo ansible-playbook wordpress.yml -i inventories/hosts -b --vault-password-file ~/.ansible_pass.txt $@